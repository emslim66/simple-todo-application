const express = require("express");
const makeStoppable = require("stoppable");
const http = require("http");

const app = express();

const server = makeStoppable(http.createServer(app));

module.exports = () => {
  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    });
  };

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log("Express server is listening on http://localhost:3000");
      resolve(stopServer);
    });
  });
};

const path = require("path");
const bodyParser = require("body-parser");
const cookieServer = require("cookie-session");

const router = require("../routes");

const TaskService = require("../services/taskService");

const taskService = new TaskService(
  "C:/Users//Zver//Desktop//homework//simple-todo-application//views//layout//partials//taskList.json"
);
const taskServiceForDone = new TaskService(
  "C:/Users//Zver//Desktop//homework//simple-todo-application//views//layout//partials//taskList2.json"
);

app.set("view engine", "ejs");
app.set(
  "views",
  path.join(
    "C:/Users//Zver//Desktop//building-a-website-with-node.js-and-express-master",
    "views"
  )
);

app.use(
  express.static(
    path.join(
      "C:/Users//Zver//Desktop//building-a-website-with-node.js-and-express-master",
      "assets"
    )
  )
);

app.use(
  cookieServer({
    name: "session",
    keys: ["dsfasfjdlf", "fkalsdfjd"],
  })
);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(
  "/",
  router({
    taskService,
    taskServiceForDone,
  })
);
