const express = require("express");

const router = express.Router();

module.exports = (params) => {
  const { taskServiceForDone } = params;
  router.get("/", async (req, res) => {
    const tasks2 = await taskServiceForDone.getList();
    res.render("layout", { template: "done", tasks2 });
  });

  return router;
};
