const express = require("express");

const router = express.Router();

const { check, validationResult } = require("express-validator");

const done = require("./done");

module.exports = (params) => {
  const { taskService, taskServiceForDone } = params;
  const validations = [
    check("task")
      .trim()
      .isLength({ min: 3 })
      .escape()
      .withMessage("Minimal length for task name is 3 letter!")
      .custom(async (value, { req }) => {
        taski = await taskService.getList();
        taski.forEach((val) => {
          if (val.taskName == value) {
            throw new Error(`Task ${value} already exists!`);
          }
          return true;
        });
      }),
  ];
  router.get("/", async (req, res) => {
    const tasks = await taskService.getList();
    const errors = req.session.task ? req.session.task.errors : false;

    req.session.task = {};
    res.render("layout", { template: "index", tasks, errors });
  });
  router.post("/", validations, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.task = {
        errors: errors.array(),
      };
      return res.redirect("/");
    }
    const { task } = req.body;

    await taskService.addEntry(task);
    return res.redirect("/");
  });

  router.post("/api/tasks/:taskName/done", async (req, res) => {
    const taskClicked = req.params.taskName;
    await taskServiceForDone.addEntry(taskClicked);
    await taskService.shiftData();

    return res.json(req.body);
  });

  router.use("/done", done(params));

  return router;
};
