const fs = require("fs");
const util = require("util");

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

class TaskService {
  constructor(datafile) {
    this.datafile = datafile;
  }
  async getList() {
    const data = await this.getData();
    return data;
  }
  async addEntry(taskName) {
    const data = (await this.getData()) || [];
    data.push({ taskName });
    return writeFile(this.datafile, JSON.stringify(data));
  }
  async getData() {
    const data = await readFile(this.datafile, "utf8");
    if (!data) return [];
    return JSON.parse(data);
  }
  async shiftData() {
    const data = (await this.getData()) || [];
    data.shift();
    return writeFile(this.datafile, JSON.stringify(data));
  }
}

module.exports = TaskService;
